# Tinyword

A very tiny (500 character) clone of a certain word guessing game. Try guessing the 5-letter word by typing in the field. Hit enter to guess. Any letters that are correct and in the correct position will be marked in green, any letters that are correct, but not in the correct place are marked in yellow.

You have six tries in total to guess the word.

Comes with a total of six words built-in!

Play on [Itch.io](https://kinami.itch.io/tinyword) or [here](https://ikinami.gitlab.io/tinyword/tinyword.html).

Entire source code can be found [here](./public/tinyword.html).

You can add your own words using a [handy tool](https://ikinami.gitlab.io/tinyword/editor.html)([source code](./public/editor.html)) (conveniently also less than 280 characters).

There is a [mini version](https://ikinami.gitlab.io/tinyword/tinyword-mini.html)([source code](./public/tinyword-mini.html)) with just 280 characters to fit into Twitter.

Entry for the [TweetTweetJam 8](https://itch.io/jam/tweettweetjam-8).

Made with ❤️ by [Kinami Imai 今井きなみ](https://ikinami.gitlab.io/kinami/)